# these are the defaults, but they can be amended/overridden in config.mk or on
# the command line (ie "make CROSS_COMPILE=some-prefix-") or even as enviornment
# vars (ie "export CROSS_COMPILE=some-prefix-; make")

# target system's root file system
SYSROOT ?= /

# cross compiler prefix
CROSS_COMPILE ?=

# installation prefix
PREFIX ?= /usr

# note: these cannot be set as env vars, and ?= does not work for many of them
# as GNU Make already sets them as so called "implicit vars"
AR = $(CROSS_COMPILE)ar
CC = $(CROSS_COMPILE)gcc

CFLAGS ?=\
 --sysroot $(SYSROOT)\
 -Wall\
 -O3

LDFLAGS ?= $(CFLAGS)

# include site specific configurations (if present)
-include config.mk

# create config.mk from template, only if config.mk does not exist yet and
# config_template.mk does
config.mk: | config_template.mk
	cp $| config.mk

#===============================================================================
# convenience items for testing and what not:

.PHONY: all clean todolist test install

.DEFAULT_GOAL = all
all: main

clean:
	rm -f *.o *.o.dep main

# note: using braces around "T" to avoid the grep being listed in the todolist
todolist:
	@grep --color=auto '[T]ODO' `find -type f`

test: all
	./main

install:\
 $(PREFIX)/bin/main

$(PREFIX)/bin/%: %
	mkdir -p `dirname $@`
	cp $< $@

#===============================================================================
# recipes:

# create program
main: $(patsubst %.c, %.o, $(shell ls *.c))

#-------------------------------------------------------------------------------
# generic recipes:

# include all foo.dep files
-include $(shell find -name "*.dep")

# disable built-in recipes
.SUFFIXES:

# create foo executable from foo.o and other object files
%: %.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# for any foo.o compile it from its foo.c file
# and generate its accompanying foo.o.dep file
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
	$(CC) $(CFLAGS) -MM -MT $@ -MF $@.dep $<

# vim: set noexpandtab :
